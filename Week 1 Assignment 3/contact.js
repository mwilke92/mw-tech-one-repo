var Contact = function (firstName, lastName, emailAdi, phoneNum) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.emailAdi = emailAdi;
    this.phoneNum = phoneNum;
}

var contacts = [];

var listContacts = function () {
    document.getElementById('displayContacts').innerHTML = " ";
    for (var i = 0; i < contacts.length; i++) {
        document.getElementById('displayContacts').innerHTML += '<tr><td id="firstName' + i + '">' + contacts[i].firstName + '</td><td id="lastName' + i + '">' + contacts[i].lastName + '</td><td id="emailAdi' + i + '">' + contacts[i].emailAdi + '</td><td id="phoneNum' + i + '">' + contacts[i].phoneNum + '</td><td></div><button class="btn btn-danger" onclick=deleteContact(' + i + ')>Delete</button></td></tr>';
    }
}

var addNewContact = function () {
    var firstName = document.getElementById('firstName').value;
    var lastName = document.getElementById('lastName').value;
    var emailAdi = document.getElementById('emailAdi').value;
    var phoneNum = document.getElementById('phoneNum').value;
    var contact = new Contact(firstName, lastName, emailAdi, phoneNum);
    contacts.push(contact);
    listContacts();
}

var deleteContact = function (i) {
    contacts.splice(i, 1);
    listContacts();
}



var submitUpdatedContact = function (i) {
    contacts[i].firstName = document.getElementById("firstName").value;
    contacts[i].lastName = document.getElementById("lastName").value;
    contacts[i].emailAdi = document.getElementById("emailAdi").value;
    contacts[i].phoneNum = document.getElementById("phoneNum").value;

    document.getElementById("firstName").value = "";
    document.getElementById("lastName").value = " ";
    document.getElementById("emailAdi").value = " ";
    document.getElementById("phoneNum").value = " ";

    listContacts();
}


listContacts();